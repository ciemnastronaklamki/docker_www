#!/bin/bash

docker rmi i_mdb i_phpa i_php_apache i_php_apache_2

docker build -t i_mdb ./mariadb
docker build -t i_phpa ./phpmyadmin
docker build -t i_php_apache ./php_apache
docker build -t i_php_apache_2 ./php_apache_2
