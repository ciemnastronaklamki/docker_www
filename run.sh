#!/bin/bash

docker stop phpm mdb apache

docker run --name mdb -d \
            --mount type=bind,source=/mnt/ext_dane/docker_mariadb_db,target=/var/lib/mysql \
            i_mdb &&

docker run --name phpm --link mdb:db -p 8080:80 -d i_phpa
docker run --name apache --link mdb:db -p 80:80 \
        --mount type=bind,source=/home/csk/repos/$1,target=/var/www/html \
        -d i_php_apache
