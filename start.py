#! python3

import os,sys, re

mo_php = re.compile(r'^php_.*')

print("Wybierz projekt:\n")

projects_path = "/home/csk/repos"
projects = []

for cont in os.listdir(projects_path):
    path = projects_path + '/' + cont
    if os.path.isdir(path):
        if mo_php.search(cont) != None:
            projects.append(cont)

i = 0
for project in projects:
    print("%d. %s" % (i, project))
    i += 1

print("\n")

wyb = input("Wybierz: ")
wyb = int(wyb)

if wyb not in range(0, len(projects)):
    sys.exit()
elif os.path.isdir(projects_path+'/'+projects[wyb]):
    os.system('sudo bash ./run.sh %s' % (projects[wyb]))
